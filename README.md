# AnyDB

AnyDB is an API used to standardize access to the database.
Its objective is to make abstraction of the underlying technology.

This project is clearly inspired by [Feathersjs](https://feathersjs.com/).
I wanted to extract this abstraction part of the database by removing the direct interconnection with the REST API.

Some of the code has been taken directly from Feathersjs ([see their GitHub](https://github.com/feathersjs/feathers)). This mainly concerns the `@feathersjs/adapter-commons` code.

**Be careful:** this project is still under development, is not yet fully tested and should not be used in a production environment at this time.
