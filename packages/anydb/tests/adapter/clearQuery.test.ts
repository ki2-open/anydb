import { cleanQuery, BadRequest } from "../../src";

describe("test clearQuery functions", () => {
  it("should directly return item (no operators)", () => {
    expect(cleanQuery(25, [])).toBe(25);
    expect(cleanQuery(true, [])).toBe(true);
    expect(cleanQuery("test", [])).toBe("test");
  });

  it("should return object (no operators)", () => {
    expect(
      cleanQuery(
        {
          data: true,
        },
        []
      )
    ).toEqual({ data: true });
  });

  it("should return array of object (no operators)", () => {
    expect(cleanQuery([{ data: true }, { value: 5 }], [])).toEqual([
      { data: true },
      { value: 5 },
    ]);
  });

  it("should validate nested (no operators", () => {
    const q1 = {
      data: {
        value: 1,
        test: true,
      },
      value: 25,
    };
    expect(cleanQuery(q1, [])).toEqual(q1);

    const q2 = {
      arr: [{ value: 1 }, { test: true }],
      test: false,
    };
    expect(cleanQuery(q2, [])).toEqual(q2);
  });

  it("should validate (valid) operators", () => {
    const op = ["$a", "$b", "$in"];

    const q1 = {
      value: 25,
      $a: true,
    };
    expect(cleanQuery(q1, op)).toEqual(q1);

    const q2 = {
      $b: {
        data: "test",
      },
    };
    expect(cleanQuery(q2, op)).toEqual(q2);

    const q3 = {
      $a: {
        $b: {
          data: "test",
        },
      },
    };
    expect(cleanQuery(q3, op)).toEqual(q3);

    const q4 = {
      $a: [{ data: "test" }, { value: 25, $b: true }],
      value: -5,
    };
    expect(cleanQuery(q4, op)).toEqual(q4);

    const q5 = {
      $a: ["a", "b"],
    };
    expect(cleanQuery(q5, op)).toEqual(q5);

    const q6 = {
      $a: [{ data: 1 }, { $b: [1, 2] }],
    };
    expect(cleanQuery(q6, op)).toEqual(q6);

    const q7 = {
      itemId: "1234",
      type: {
        $in: ["a", "b"],
      },
    };
    expect(cleanQuery(q7, op)).toEqual(q7);
  });

  it("should throw bad request (invalid operator)", () => {
    const t = () => {
      return cleanQuery({ $a: "test" }, []);
    };
    expect(t).toThrow(BadRequest);
  });

  it("should ignore invalid operator", () => {
    const op = ["$a", "b"];
    const q1 = {
      $a: true,
      b: 25,
    };
    expect(cleanQuery(q1, op)).toEqual(q1);

    const t = () => {
      return cleanQuery({ $b: true }, op);
    };
    expect(t).toThrow(BadRequest);
  });
});
