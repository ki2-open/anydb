import { AnyDbAdapter, BASE_OPERATORS, NotImplemented } from "../../src";

describe("test AnyDB base adapter", () => {
  it("should construct correct object", () => {
    const adapter = new AnyDbAdapter({});
    expect(adapter.id).toBe("id");
    expect(adapter.options.id).toBe("id");
    expect(adapter.options.allowedOperators).toEqual(BASE_OPERATORS);

    const adapter2 = new AnyDbAdapter({ id: "_id" });
    expect(adapter2.id).toBe("_id");

    const adapter3 = new AnyDbAdapter({ allowedOperators: [] });
    expect(adapter3.options.allowedOperators).toEqual([]);
  });

  it("should throw NotImplemented errors", () => {
    const adapter = new AnyDbAdapter({});

    const tfind = () => {
      return adapter.find({});
    };

    expect(tfind).toThrow(NotImplemented);

    const tcount = () => {
      return adapter.count({});
    };

    expect(tcount).toThrow(NotImplemented);

    const tget = () => {
      return adapter.get(0);
    };

    expect(tget).toThrow(NotImplemented);

    const tcreateOne = () => {
      return adapter.createOne({});
    };

    expect(tcreateOne).toThrow(NotImplemented);

    const tcreateMany = () => {
      return adapter.createMany([]);
    };

    expect(tcreateMany).toThrow(NotImplemented);

    const tupdate = () => {
      return adapter.update(0, {});
    };

    expect(tupdate).toThrow(NotImplemented);

    const tpatchOne = () => {
      return adapter.patchOne(0, {});
    };

    expect(tpatchOne).toThrow(NotImplemented);

    const tpatchMany = () => {
      return adapter.patchMany({}, {});
    };

    expect(tpatchMany).toThrow(NotImplemented);

    const tremoveOne = () => {
      return adapter.removeOne(0);
    };

    expect(tremoveOne).toThrow(NotImplemented);

    const tremoveMany = () => {
      return adapter.removeMany({});
    };

    expect(tremoveMany).toThrow(NotImplemented);
  });
});
