import { AnyDbError } from "../../src";

describe("test base AnyDB errors", () => {
  it("should throw AnyDbError", () => {
    const t = () => {
      throw new AnyDbError("name", "message", "classname", undefined);
    };

    expect(t).toThrow(AnyDbError);
  });

  it("shoudl not thrown AnyDbError", () => {
    const t = () => {
      throw new Error("message");
    };

    expect(t).not.toThrow(AnyDbError);
  });

  it("should contain correct data", () => {
    const error = new AnyDbError("message", "name", "classname", {
      data: 1,
    });

    expect(error.message).toEqual("message");
    expect(error.name).toEqual("name");
    expect(error.className).toEqual("classname");
    expect(error.data).toEqual({ data: 1 });
    expect(error.type).toEqual("AnyDbError");
  });

  it("should contain default data", () => {
    const error = new AnyDbError(undefined, "name", "classname", undefined);
    expect(error.message).toEqual("Error");
  });

  it("should return valid JSON (no nested data)", () => {
    const error = new AnyDbError("msg", "name", "cn", null);
    expect(error.toJSON()).toEqual({
      name: "name",
      message: "msg",
      className: "cn",
    });
  });

  it("should return valid JSON (nested data)", () => {
    const error = new AnyDbError("msg", "name", "cn", { data: 1 });
    expect(error.toJSON()).toEqual({
      name: "name",
      message: "msg",
      className: "cn",
      data: {
        data: 1,
      },
    });
  });
});
