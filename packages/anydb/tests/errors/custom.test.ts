import { NotImplemented, NotFound, BadRequest } from "../../src";

describe("test custom errors", () => {
  it("should throw NotImplemented", () => {
    const t = () => {
      throw new NotImplemented("message");
    };

    expect(t).toThrow(NotImplemented);
  });

  it("should return valid NotImplemented JSON", () => {
    const error = new NotImplemented("message");
    expect(error.toJSON()).toEqual({
      name: "NotImplemented",
      message: "message",
      className: "not-implemented",
    });
  });

  it("should throw NotFound", () => {
    const t = () => {
      throw new NotFound("message");
    };
    expect(t).toThrow(NotFound);
  });

  it("should return valid NotFound JSON", () => {
    const error = new NotFound("message");
    expect(error.toJSON()).toEqual({
      name: "NotFound",
      message: "message",
      className: "not-found",
    });
  });

  it("should throw BadRequest", () => {
    const t = () => {
      throw new BadRequest("message");
    };
    expect(t).toThrow(BadRequest);
  });

  it("should return valid BadRequest JSON", () => {
    const error = new BadRequest("message");
    expect(error.toJSON()).toEqual({
      name: "BadRequest",
      message: "message",
      className: "bad-request",
    });
  });
});
