import { Collections, ICollections, AnyDbAdapter } from "../../src";

declare module "../../src" {
  interface ICollections {
    test: AnyDbAdapter;
  }
}

describe("test collections", () => {
  it("should create & get collection", () => {
    const anydb = new Collections();
    const adapter = new AnyDbAdapter({});
    anydb.add("test", adapter);
    expect(anydb.collection("test")).toBe(adapter);
  });
});
