import { isFilterKey } from "../../src";

describe("test commons functions", () => {
  it("should validate isFilterKey", () => {
    expect(isFilterKey("$test")).toBe(true);
    expect(isFilterKey("test")).toBe(false);
  });
});
