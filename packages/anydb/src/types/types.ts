export type Id = number | string;
export type NullableId = Id | null;

export interface Query {
  [key: string]: QueryItem;
}

export type BaseQueryItem =
  | number
  | string
  | boolean
  | bigint
  | Date
  | undefined
  | null;
export type QueryItem = Query | BaseQueryItem | Array<QueryItem>;

export interface Filters {
  select?: any;
  sort?: any;
  limit?: any;
  skip?: any;
  [key: string]: any | undefined;
}

export interface Params {
  filters?: Filters;
  options?: any;
  return?: boolean;
  [key: string]: any;
}

export interface ParamsWithoutReturn extends Params {
  return: false;
}

export interface ParamsWithReturn extends Params {
  return: true;
}

export interface AnyDbMethods<T, D = Partial<T>> {
  find(query: Query, params?: Params): Promise<T[]>;

  count(query: Query, params?: Params): Promise<number>;

  get(id: Id, params?: Params): Promise<T | null>;

  createOne(data: D, params: ParamsWithoutReturn): Promise<Id | null>;
  createOne(data: D, params: ParamsWithReturn): Promise<T | null>;
  createOne(data: D, params?: Params): Promise<T | null>;
  createMany(data: D[], params: ParamsWithoutReturn): Promise<Id[]>;
  createMany(data: D[], params: ParamsWithReturn): Promise<T[]>;
  createMany(data: D[], params?: Params): Promise<T[]>;

  update(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  update(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  update(id: Id, data: D, params?: Params): Promise<void>;

  patchOne(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchOne(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  patchOne(id: Id, data: D, params?: Params): Promise<void>;
  patchMany(query: Query, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchMany(query: Query, data: D, params: ParamsWithReturn): Promise<T[]>;
  patchMany(query: Query, data: D, params?: Params): Promise<void>;

  removeOne(id: Id, params: ParamsWithoutReturn): Promise<void>;
  removeOne(id: Id, params: ParamsWithReturn): Promise<T | null>;
  removeOne(id: Id, params?: Params): Promise<void>;
  removeMany(query: Query, params: ParamsWithoutReturn): Promise<void>;
  removeMany(query: Query, params: ParamsWithReturn): Promise<T[]>;
  removeMany(query: Query, params?: Params): Promise<void>;
}
