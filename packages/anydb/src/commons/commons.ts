export function isFilterKey(key: string): boolean {
  return key.charAt(0) === "$";
}
