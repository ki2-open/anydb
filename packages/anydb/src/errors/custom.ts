import { AnyDbError } from ".";
import type { ErrorMessage } from ".";

export class NotImplemented extends AnyDbError {
  constructor(message?: ErrorMessage, data?: any) {
    super(message, "NotImplemented", "not-implemented", data);
  }
}

export class NotFound extends AnyDbError {
  constructor(message?: ErrorMessage, data?: any) {
    super(message, "NotFound", "not-found", data);
  }
}

export class BadRequest extends AnyDbError {
  constructor(message?: ErrorMessage, data?: any) {
    super(message, "BadRequest", "bad-request", data);
  }
}
