import { exist, isString } from "@ki2/utils";

export interface AnyDbErrorJSON {
  name: string;
  message: string;
  className: string;
  data?: any;
}

export type ErrorMessage = string;

export class AnyDbError extends Error {
  readonly type: string;
  readonly className!: string;
  readonly data: any;

  constructor(
    msg: ErrorMessage | undefined,
    name: string,
    className: string,
    data: any
  ) {
    super();
    this.message = "Error";
    if (isString(msg)) {
      this.message = msg;
    }

    this.name = name;
    this.className = className;
    this.type = "AnyDbError";

    this.data = data;
  }

  toJSON() {
    const result: AnyDbErrorJSON = {
      name: this.name,
      message: this.message,
      className: this.className,
    };

    if (exist(this.data)) {
      result.data = this.data;
    }

    return result;
  }
}
