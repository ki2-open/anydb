import {
  each,
  isArray,
  isUndefined,
  isStrictObject,
  isNumber,
  isString,
  isBoolean,
  isBigInt,
  isNull,
} from "@ki2/utils";

import { BadRequest } from "../errors";

import type { QueryItem, Query, BaseQueryItem } from "../types";
import { isFilterKey } from "../commons";

function isBaseQueryItem(data: any): data is BaseQueryItem {
  return (
    isNumber(data) ||
    isString(data) ||
    isBoolean(data) ||
    isBigInt(data) ||
    isNull(data) ||
    isUndefined(data) ||
    data instanceof Date
  );
}

function cleanSubQuery(
  subquery: Query | BaseQueryItem,
  allowedOperators: string[]
): QueryItem {
  if (isBaseQueryItem(subquery)) {
    return subquery;
  }

  const result: Query = {};

  // clean each query item
  each(subquery, (value, key) => {
    if (isFilterKey(key)) {
      if (!allowedOperators.includes(key)) {
        throw new BadRequest(`Invalid query parameter ${key}`, subquery);
      }
    }
    result[key] = cleanQuery(value, allowedOperators);
  });

  // Copy symbols to result object
  Object.getOwnPropertySymbols(subquery).forEach((symbol) => {
    //@ts-ignore
    result[symbol] = subquery[symbol];
  });

  return result;
}

function cleanSubQueries(
  subqueries: QueryItem[],
  allowedOperators: string[]
): QueryItem[] {
  const result: QueryItem[] = [];
  for (const subquery of subqueries) {
    const cleaned = cleanQuery(subquery, allowedOperators);
    if (!isUndefined(cleaned)) {
      result.push(cleaned);
    }
  }
  return result;
}

/** cleanQuery
 *
 * Recursively clean each item of a query :
 *  - array are cleaned item by item
 *  - valid objects are cleaned key by key
 *  - others types are skiped
 *
 * @param queryItem
 * @param operators
 * @param filters
 * @returns
 */
export function cleanQuery(query: QueryItem, allowedOperators: string[]): any {
  if (isArray(query)) {
    return cleanSubQueries(query, allowedOperators);
  } else if (isStrictObject(query)) {
    return cleanSubQuery(query, allowedOperators);
  }
  return query;
}
