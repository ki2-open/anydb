import type {
  AnyDbMethods,
  Query,
  Params,
  ParamsWithReturn,
  ParamsWithoutReturn,
  Id,
} from "../types";
import { NotImplemented } from "../errors";

export interface AnyDbAdapterOptions {
  id: string;
  allowedOperators: string[];
}

export const BASE_OPERATORS = [
  "$in",
  "$nin",
  "$lt",
  "$lte",
  "$gt",
  "$gte",
  "$ne",
  "$or",
];

/** TODO
 *
 * Do not return created, modified or removed object by default.
 *
 * Add function to return created, modified or removed object. Can
 * use a parameter to if the object is returned.
 *
 * The aims is to avoid unecessary extra call to database when not
 * requested by the library user.
 *
 */

export class AnyDbAdapter<T = any, D = Partial<T>>
  implements AnyDbMethods<T, D> {
  options: AnyDbAdapterOptions;

  constructor(options: Partial<AnyDbAdapterOptions>) {
    this.options = {
      id: "id",
      allowedOperators: BASE_OPERATORS,
      ...options,
    };
  }

  get id() {
    return this.options.id;
  }

  find(query: Query, params?: Params): Promise<T[]> {
    throw new NotImplemented();
  }

  count(query: Query, params?: Params): Promise<number> {
    throw new NotImplemented();
  }

  get(id: Id, params?: Params): Promise<T | null> {
    throw new NotImplemented();
  }

  createOne(data: D, params: ParamsWithoutReturn): Promise<Id | null>;
  createOne(data: D, params: ParamsWithReturn): Promise<T | null>;
  createOne(data: D, params?: Params): Promise<T | null>;
  createOne(data: D, params?: Params): Promise<T | Id | null> {
    throw new NotImplemented();
  }

  createMany(data: D[], params: ParamsWithoutReturn): Promise<Id[]>;
  createMany(data: D[], params: ParamsWithReturn): Promise<T[]>;
  createMany(data: D[], params?: Params): Promise<T[]>;
  createMany(data: D[], params?: Params): Promise<T[] | Id[]> {
    throw new NotImplemented();
  }

  update(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  update(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  update(id: Id, data: D, params?: Params): Promise<void>;
  update(id: Id, data: D, params?: Params): Promise<T | null | void> {
    throw new NotImplemented();
  }

  patchOne(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchOne(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  patchOne(id: Id, data: D, params?: Params): Promise<void>;
  patchOne(id: Id, data: D, params?: Params): Promise<T | null | void> {
    throw new NotImplemented();
  }

  patchMany(query: Query, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchMany(query: Query, data: D, params: ParamsWithReturn): Promise<T[]>;
  patchMany(query: Query, data: D, params?: Params): Promise<void>;
  patchMany(query: Query, data: D, params?: Params): Promise<T[] | void> {
    throw new NotImplemented();
  }

  removeOne(id: Id, params: ParamsWithoutReturn): Promise<void>;
  removeOne(id: Id, params: ParamsWithReturn): Promise<T | null>;
  removeOne(id: Id, params?: Params): Promise<void>;
  removeOne(id: Id, params?: Params): Promise<T | null | void> {
    throw new NotImplemented();
  }

  removeMany(query: Query, params: ParamsWithoutReturn): Promise<void>;
  removeMany(query: Query, params: ParamsWithReturn): Promise<T[]>;
  removeMany(query: Query, params?: Params): Promise<void>;
  removeMany(query: Query, params?: Params): Promise<T[] | void> {
    throw new NotImplemented();
  }
}
