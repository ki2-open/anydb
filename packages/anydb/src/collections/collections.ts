import type { AnyDbAdapter } from "../adapter";

export interface ICollections {}

export class Collections {
  private _collections: Map<string, AnyDbAdapter> = new Map<
    string,
    AnyDbAdapter
  >();

  add(name: string, service: AnyDbAdapter) {
    this._collections.set(name, service);
  }

  collection<L extends keyof ICollections>(name: L): ICollections[L] {
    return this._collections.get(name) as ICollections[L];
  }
}
