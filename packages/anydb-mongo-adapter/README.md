# AnyDB MongoDB adapter

MongoDB adapter for AnyDB (`@ki2/anydb`).

This adapter is a version for AnyDB of the MongoDB adapter code for Feathersjs ([see original sources](https://github.com/feathersjs-ecosystem/feathers-mongodb)).
