import { ObjectId } from "mongodb";
import type { Document } from "mongodb";
import { exist, extend, isEmpty, omit, isNumber, isString } from "@ki2/utils";
import { isFilterKey } from "@ki2/anydb";
import type { Id, NullableId, Params } from "@ki2/anydb";

import type {
  MongoAdapterOptions,
  MongoFilters,
  ExtId,
  NullableExtId,
} from "./types";

export function isId(value: unknown): value is Id {
  if (isNumber(value) || isString(value)) {
    return true;
  }
  return false;
}

export function isExtId(value: unknown): value is ExtId {
  if (isId(value)) {
    return true;
  }
  if (ObjectId.isValid(value as any)) {
    return true;
  }
  return false;
}

/** useIdKey
 *
 * Get the key value used for the id
 *
 * @param options
 * @returns
 */
export function useIdKey<T extends Document>(options: MongoAdapterOptions<T>) {
  return options.id;
}

/** objectifyId
 *
 * Convert id to ObjectId if id key is '_id'.
 *
 * @param id
 * @param options
 * @returns
 */
export function objectifyId<T extends Document>(
  id: Id,
  options: MongoAdapterOptions<T>
) {
  const idk = useIdKey(options);
  let oid: Id | ObjectId = id;

  if (options.disableObjectify) {
    return oid;
  }

  if (idk === "_id" && ObjectId.isValid(oid)) {
    oid = new ObjectId(oid);
  }

  return oid;
}

export function simplifyId(id: NullableExtId): NullableId {
  if (isId(id)) {
    return id;
  }
  if (!exist(id)) {
    return null;
  }
  return String(id);
}

/** normalizeId
 *
 * @param id
 * @param data
 * @param options
 * @returns
 */
export function normalizeId<T extends Document>(
  id: NullableId,
  data: any,
  options: MongoAdapterOptions<T>
) {
  const idk = useIdKey(options);

  if (idk === "_id") {
    // Default Mongo IDs cannot be updated. The Mongo library handles
    // this automatically.
    return omit(data, idk);
  } else if (id !== null) {
    // If not using the default Mongo _id field set the ID to its
    // previous value. This prevents orphaned documents.
    return extend({}, data, { [idk]: id });
  } else {
    return data;
  }
}

/** remapModifiers
 *
 * Map stray records into $set
 *
 * @param data
 * @returns
 */
export function remapModifiers(data: any) {
  var set: any = {};
  // Step through the root
  for (var key in data) {
    // Check for keys that aren't modifiers
    if (!isFilterKey(key)) {
      // Move them to set, and remove their record
      set[key] = data[key];
      delete data[key];
    }
    // If the '$set' modifier is used, add that to the temp variable
    if (key === "$set") {
      set = extend(set, data[key]);
      delete data[key];
    }
  }
  // If we have a $set, then attach to the data object
  if (!isEmpty(set)) {
    data.$set = set;
  }
  return data;
}

/** setId
 *
 * Generate a MongoDB id if we use a custom id field
 *
 * @param item
 * @param options
 * @returns
 */
export function setId<T extends Document>(
  item: any,
  options: MongoAdapterOptions<T>
): any {
  const idk = useIdKey(options);
  const entry = extend({}, item);

  if (idk !== "_id" && !exist(entry[idk])) {
    entry[idk] = new ObjectId().toHexString();
  }

  return entry;
}

export function useFilters(params?: Params): MongoFilters {
  return params?.filters ?? {};
}

export function useOptions(params?: Params): any {
  return params?.options;
}
