import type { Document, Filter } from "mongodb";
import { exist, extend, isArray } from "@ki2/utils";
import { AnyDbAdapter, NotFound, cleanQuery } from "@ki2/anydb";
import type {
  Id,
  Query,
  Params,
  ParamsWithReturn,
  ParamsWithoutReturn,
} from "@ki2/anydb";

import type { IModel, MongoAdapterOptions } from "./types";
import {
  objectifyId,
  normalizeId,
  remapModifiers,
  setId,
  useFilters,
  useOptions,
  simplifyId,
} from "./helpers";

export class MongoAdapter<
  T extends Document = Document,
  D = Partial<T>
> extends AnyDbAdapter<T, D> {
  constructor(options: Partial<MongoAdapterOptions<T>>) {
    if (!exist(options.Model)) {
      throw new Error("MongoDB Model (collection) required");
    }

    super({
      id: "_id",
      ...options,
    });
  }

  get extoptions(): MongoAdapterOptions<T> {
    return this.options as MongoAdapterOptions<T>;
  }

  get Model(): IModel<T> {
    return this.extoptions.Model;
  }

  set Model(model: IModel<T>) {
    this.extoptions.Model = model;
  }

  async find(query: Query = {}, params?: Params): Promise<T[]> {
    const filters = useFilters(params);
    const options = useOptions(params);

    const fquery = cleanQuery(query, this.options.allowedOperators) as any;

    if (fquery[this.id]) {
      fquery[this.id] = objectifyId(fquery[this.id], this.extoptions);
    }

    const q = this.Model.find(fquery as Filter<T>, options);

    if (filters.select) {
      let select = filters.select;
      if (isArray(select)) {
        let tmp: any = {};
        select.forEach((name) => {
          tmp[name] = 1;
        });
        select = tmp;
      }
      q.project(select);
    }

    if (filters.sort) {
      q.sort(filters.sort);
    }

    if (filters.collation) {
      q.collation(filters.collation);
    }

    if (filters.hint) {
      q.hint(filters.hint);
    }

    if (filters.limit) {
      q.limit(filters.limit);
    }

    if (filters.skip) {
      q.skip(filters.skip);
    }

    return (await q.toArray()) as T[];
  }

  async count(query: Query, params?: Params): Promise<number> {
    const options = useOptions(params);
    query = cleanQuery(query, this.options.allowedOperators);

    return await this.Model.countDocuments(query as Filter<T>, options);
  }

  async get(id: Id, params?: Params): Promise<T | null> {
    const options = useOptions(params);

    const query: any = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    const result = (await this.Model.findOne(
      query as Filter<T>,
      options
    )) as T | null;
    if (!exist) {
      throw new NotFound(`[DB] No record found for id ${id}`);
    }
    return result ?? null;
  }

  createOne(data: D, params: ParamsWithoutReturn): Promise<Id | null>;
  createOne(data: D, params: ParamsWithReturn): Promise<T | null>;
  createOne(data: D, params?: Params): Promise<T | null>;
  async createOne(data: D, params?: Params): Promise<T | Id | null> {
    const options = useOptions(params);
    const entry = setId(data, this.extoptions);
    const result = await this.Model.insertOne(entry, options);
    if (result.acknowledged !== true) {
      return null;
    }
    const id = simplifyId(result.insertedId);
    if (!exist(id)) {
      return null;
    }
    if (params?.return === true) {
      return this.get(id);
    }
    return id;
  }

  createMany(data: D[], params: ParamsWithoutReturn): Promise<Id[]>;
  createMany(data: D[], params: ParamsWithReturn): Promise<T[]>;
  createMany(data: D[], params?: Params): Promise<T[]>;
  async createMany(data: D[], params?: Params): Promise<T[] | Id[]> {
    const options = useOptions(params);
    const entries = data.map((item) => setId(item, this.extoptions));
    const result = await this.Model.insertMany(entries, options);
    if (result.acknowledged !== true) {
      return [];
    }
    if (params?.return === true) {
      const parr: Array<Promise<T | null>> = [];
      for (const idx in result.insertedIds) {
        const id = simplifyId(result.insertedIds[idx]);
        if (!exist(id)) {
          continue;
        }
        const p = this.get(id);
        parr.push(p);
      }

      const items = await Promise.all(parr);
      return items.filter((item) => exist(item)) as T[];
    }

    const ids: Id[] = [];
    for (const idx in result.insertedIds) {
      const id = simplifyId(result.insertedIds[idx]);
      if (!exist(id)) {
        continue;
      }
      ids.push(id);
    }
    return ids;
  }

  update(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  update(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  update(id: Id, data: D, params?: Params): Promise<void>;
  async update(id: Id, data: D, params?: Params): Promise<T | null | void> {
    const options = useOptions(params);

    const query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    const normalizeddata = normalizeId(id, data, this.extoptions);

    await this.Model.replaceOne(query as Filter<T>, normalizeddata, options);
    if (params?.return === true) {
      return await this.get(id, params);
    }
    return;
  }

  patchOne(id: Id, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchOne(id: Id, data: D, params: ParamsWithReturn): Promise<T | null>;
  patchOne(id: Id, data: D, params?: Params): Promise<void>;
  async patchOne(id: Id, data: D, params?: Params): Promise<T | null | void> {
    const options = useOptions(params);
    const filters = useFilters(params);

    let query: any = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const normalizeddata = normalizeId(id, data, this.extoptions);
    const remapedModifier = remapModifiers(normalizeddata);

    await this.Model.updateOne(query as Filter<T>, remapedModifier, options);
    if (params?.return === true) {
      return await this.get(id);
    }
    return;
  }

  patchMany(query: Query, data: D, params: ParamsWithoutReturn): Promise<void>;
  patchMany(query: Query, data: D, params: ParamsWithReturn): Promise<T[]>;
  patchMany(query: Query, data: D, params?: Params): Promise<void>;
  async patchMany(query: Query, data: D, params?: Params): Promise<T[] | void> {
    const options = useOptions(params);
    const filters = useFilters(params);

    query = cleanQuery(query, this.options.allowedOperators);

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    const normalizeddata = normalizeId(null, data, this.extoptions);
    const remapedModifier = remapModifiers(normalizeddata);

    const results: any[] = await this.find(query, params);
    const ids = results.map((item) => item[this.id]);

    await this.Model.updateMany(query as Filter<T>, remapedModifier, options);

    if (params?.return === true) {
      return await this.find({
        [this.id]: { $in: ids },
      });
    }
    return;
  }

  removeOne(id: Id, params: ParamsWithoutReturn): Promise<void>;
  removeOne(id: Id, params: ParamsWithReturn): Promise<T | null>;
  removeOne(id: Id, params?: Params): Promise<void>;
  async removeOne(id: Id, params?: Params): Promise<T | null | void> {
    const options = useOptions(params);
    const filters = useFilters(params);

    let query = {
      [this.id]: objectifyId(id, this.extoptions),
    };

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    let item: T | null | undefined = undefined;
    const withreturn = params?.return ?? false;
    if (withreturn) {
      item = await this.get(id, params);
    }

    await this.Model.deleteOne(query as Filter<T>, options);

    if (withreturn) {
      return item ?? null;
    }
    return;
  }

  removeMany(query: Query, params: ParamsWithoutReturn): Promise<void>;
  removeMany(query: Query, params: ParamsWithReturn): Promise<T[]>;
  removeMany(query: Query, params?: Params): Promise<void>;
  async removeMany(query: Query, params?: Params): Promise<T[] | void> {
    const options = useOptions(params);
    const filters = useFilters(params);

    query = cleanQuery(query, this.options.allowedOperators);

    if (filters.collation) {
      query = extend(query, { collation: filters.collation });
    }

    let items: T[] | undefined = undefined;
    const withreturn = params?.return ?? false;

    if (withreturn) {
      items = await this.find(query, params);
    }

    await this.Model.deleteMany(query as Filter<T>, options);

    if (withreturn) {
      return items;
    }
    return;
  }
}
