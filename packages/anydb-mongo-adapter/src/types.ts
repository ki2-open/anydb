import type { Collection as IModel, Document, ObjectId } from "mongodb";
import type { AnyDbAdapterOptions, Filters, Id } from "@ki2/anydb";

//import type { IModel } from "./model";

export interface MongoAdapterOptions<T extends Document = Document>
  extends AnyDbAdapterOptions {
  Model: IModel<T>;
  disableObjectify?: boolean;
}

export interface MongoFilters extends Filters {
  hint?: any;
  collation?: any;
}

export type ExtId = Id | ObjectId;
export type NullableExtId = ExtId | null;

export { IModel };
